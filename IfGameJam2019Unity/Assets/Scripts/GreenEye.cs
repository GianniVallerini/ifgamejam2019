﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenEye : MonoBehaviour
{
    public float minAcc = 0f;
    public float maxAcc = .5f;
    public List<GameObject> activableObjects = new List<GameObject>();

    private void Start()
    {
        AccelerationManager.instance.AccelerationChanged += UpdateGraphics;
    }

    private void UpdateGraphics(float normalizedAcceleration)
    {
        if(normalizedAcceleration <= maxAcc && normalizedAcceleration >= minAcc)
        {
            foreach(GameObject obj in activableObjects)
            {
                obj.SetActive(true);
            }
        }
        else
        {
            foreach (GameObject obj in activableObjects)
            {
                obj.SetActive(false);
            }
        }
    }
}
