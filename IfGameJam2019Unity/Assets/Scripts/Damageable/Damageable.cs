﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damageable : IDamageable
{
    #region Fields

    public event Action Destroyed;
    public event Action<int,int> Damaged;

    private int maxHpPossible;
    private int currHp;

    #endregion

    #region Methods

    public Damageable(int maxHp)
    {
        maxHpPossible = maxHp;
        currHp = maxHp;
    }

    public void TakeDamage(int damage)
    {
        if (currHp > 0)
        {
            currHp -= damage;
            if (currHp <= 0)
            {
                Destroyed?.Invoke();
            }
            else
            {
                Damaged?.Invoke(currHp, maxHpPossible);
            }
        }
    }

    public void InstaDestroy()
    {
        if (currHp > 0)
        {
            currHp = 0;
            Destroyed?.Invoke();
        }
    }

    #endregion
}
