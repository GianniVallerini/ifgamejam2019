using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[RequireComponent(typeof(Collider2D))]
public class Damager : MonoBehaviour
{
    #region Fields

    public bool destroyOnDamage = false;
    public bool isInstaDestroy = false;
    public bool hitsOnCollision = true;
    [HideIf("isInstaDestroy")]
    public int damage = 1;
    public LayerMask iDamageableMask;
    [ShowIf("destroyOnDamage")] public LayerMask iDieWhenHitMask;

    #endregion

    #region Callbacks

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (hitsOnCollision)
        {
            int objectHitLayer = collision.gameObject.layer;
            if (LayerRecon.IsInLayerMask(objectHitLayer, iDamageableMask))
            {
                IDamageable iDamageable = collision.gameObject.GetComponent<IDamageable>();
                if (iDamageable == null)
                {
                    iDamageable = collision.gameObject.GetComponentInParent<IDamageable>();
                }
                if (iDamageable != null)
                {
                    DealDamage(iDamageable, false);
                }
            }

            if (LayerRecon.IsInLayerMask(objectHitLayer, iDieWhenHitMask))
            {
                Die();
            }
        }
    }

    #endregion

    #region Methods

    public void DealDamage(IDamageable iDamageable, bool dieAfterThisCollision = false)
    {
        if (isInstaDestroy)
        {
            iDamageable.InstaDestroy();
        }
        else
        {
            iDamageable.TakeDamage(damage);
        }

        if (dieAfterThisCollision)
        {
            Die();
        }
    }

    public void Die()
    {
        gameObject.SetActive(false);
    }

    public void DealDamage(List<IDamageable> iDamageables, bool dieAfterThisCollision = false)
    {
        foreach (IDamageable damageable in iDamageables)
        {
            if (isInstaDestroy)
            {
                damageable.InstaDestroy();
            }
            else
            {
                damageable.TakeDamage(damage);
            }
        }

        if (dieAfterThisCollision)
        {
            Die();
        }
    }

    #endregion
}

public static class LayerRecon
{
    public static bool IsInLayerMask(int layer, LayerMask mask)
    {
        return mask == (mask | (1 << layer));
    }
}
