﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraphicEffects : MonoBehaviour
{
    public SpriteRenderer greenFade;
    public SpriteRenderer violetFade;

    private void Start()
    {
        AccelerationManager.instance.AccelerationChanged += UpdateAccelerometerSlider;
    }

    private void UpdateAccelerometerSlider(float normalizedValue)
    {
        Color greenColor = greenFade.color;
        greenColor.a = Mathf.InverseLerp(.5f, 0f, normalizedValue);
        greenFade.color = greenColor;

        Color violetColor = violetFade.color;
        violetColor.a = Mathf.InverseLerp(.5f, 1f, normalizedValue);
        violetFade.color = violetColor;
    }
}
