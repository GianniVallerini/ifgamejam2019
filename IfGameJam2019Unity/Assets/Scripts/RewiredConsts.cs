// <auto-generated>
// Rewired Constants
// This list was generated on 12/10/2019 12:14:44
// The list applies to only the Rewired Input Manager from which it was generated.
// If you use a different Rewired Input Manager, you will have to generate a new list.
// If you make changes to the exported items in the Rewired Input Manager, you will
// need to regenerate this list.
// </auto-generated>

namespace RewiredConsts {
    public static partial class Action {
        // Default
        // Gameplay
        [Rewired.Dev.ActionIdFieldInfo(categoryName = "Gameplay", friendlyName = "Horizontal Movement")]
        public const int Horizontal_Movement = 0;
        [Rewired.Dev.ActionIdFieldInfo(categoryName = "Gameplay", friendlyName = "Vertical Movement")]
        public const int Vertical_Movement = 1;
        [Rewired.Dev.ActionIdFieldInfo(categoryName = "Gameplay", friendlyName = "Gravity Scroll")]
        public const int Gravity_Scroll = 2;
        [Rewired.Dev.ActionIdFieldInfo(categoryName = "Gameplay", friendlyName = "Grapple")]
        public const int Grapple = 3;
        // Menu
    }
    public static partial class Category {
        public const int Default = 0;
        public const int Gameplay = 1;
        public const int Menu = 2;
    }
    public static partial class Layout {
        public static partial class Joystick {
            public const int Default = 0;
        }
        public static partial class Keyboard {
            public const int Default = 0;
            public const int Layout0 = 1;
        }
        public static partial class Mouse {
            public const int Default = 0;
            public const int Layout0 = 1;
        }
        public static partial class CustomController {
            public const int Default = 0;
        }
    }
    public static partial class Player {
        [Rewired.Dev.PlayerIdFieldInfo(friendlyName = "System")]
        public const int System = 9999999;
        [Rewired.Dev.PlayerIdFieldInfo(friendlyName = "Player0")]
        public const int Player0 = 0;
    }
    public static partial class CustomController {
    }
    public static partial class LayoutManagerRuleSet {
    }
    public static partial class MapEnablerRuleSet {
    }
}
