﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour
{
    #region Fields

    public GameObject target;
    public float followSpeed = 1f;
    public bool followX = true;
    public bool followY = true;
    public Vector2 minMaxX = Vector2.zero;
    public Vector2 minMaxY = Vector2.zero;

    private bool switchingBounds = false;

    #endregion

    #region Unity Callbacks

    private void Start()
    {
        CheckPointManager.instance.Respawned += ResetPosition;

        Room[] rooms = FindObjectsOfType<Room>();
        foreach(Room room in rooms)
        {
            room.RoomActivated += PassRoomValues;
        }

        ResetPosition();
    }

    private void FixedUpdate()
    {
        Follow();
    }

    #endregion

    #region Methods

    public void ResetPosition()
    {
        if (target != null)
        {
            transform.position = new Vector3(target.transform.position.x, transform.position.y, transform.position.z);
        }
    }

    private void Follow()
    {
        if (target != null)
        {
            Vector3 targetDirection = target.transform.position - transform.position;

            if (followX)
            {
                transform.position += new Vector3(targetDirection.x, 0, 0) * followSpeed * Time.fixedDeltaTime;
                if(transform.position.x < minMaxX.x)
                {
                    transform.position = new Vector3(minMaxX.x, transform.position.y, transform.position.z);
                }
                if (transform.position.x > minMaxX.y)
                {
                    transform.position = new Vector3(minMaxX.y, transform.position.y, transform.position.z);
                }
            }

            if (followY)
            {
                transform.position = new Vector3(0, targetDirection.y, 0) * followSpeed * Time.fixedDeltaTime;
                if (transform.position.y < minMaxY.x)
                {
                    transform.position = new Vector3(transform.position.x, minMaxY.x, transform.position.z);
                }
                if (transform.position.y > minMaxY.y)
                {
                    transform.position = new Vector3(transform.position.x, minMaxY.y, transform.position.z);
                }
            }
        }
    }

    private void PassRoomValues(Room room)
    {
        if (!switchingBounds)
        {
            StartCoroutine(ChangeBounds(room.boundsX, room.boundsY));
        }
    }

    private IEnumerator ChangeBounds(Vector2 xBounds, Vector2 yBounds, float time = 4f)
    {
        switchingBounds = true;
        Vector2 startingXBound = minMaxX;
        Vector2 startingYBound = minMaxY;
        float t = 0;
        float maxT = time;
        float a = 0;
        while(t < maxT)
        {
            t += Time.deltaTime;
            a = t / maxT;
            minMaxX.x = Mathf.Lerp(minMaxX.x, xBounds.x, a);
            minMaxX.y = Mathf.Lerp(minMaxX.y, xBounds.y, a);
            minMaxY.x = Mathf.Lerp(minMaxY.x, yBounds.x, a);
            minMaxY.y = Mathf.Lerp(minMaxY.y, yBounds.y, a);
            yield return null;
        }
        minMaxX = xBounds;
        minMaxY = yBounds;
        switchingBounds = false;
    }

    #endregion
}
