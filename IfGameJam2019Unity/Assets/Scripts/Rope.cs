﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rope : Unlocker, IDamageable
{
    #region Fields

    public string breakSoundKey = "";

    private Damageable damageable = new Damageable(1);
    private bool isBreaking = false;

    #endregion

    #region Unity Callbacks

    #endregion

    #region Methods

    public void InstaDestroy()
    {
        damageable.InstaDestroy();
        Break();
        CallUnlock();
    }

    public void TakeDamage(int damage)
    {
        damageable.TakeDamage(damage);
    }

    private void Break()
    {
        if (!isBreaking)
        {
            CallUnlock();
            StartCoroutine(DestroyCor());
        }
    }

    private IEnumerator DestroyCor()
    {
        isBreaking = true;
        if (breakSoundKey != "")
        {
            SoundsDatabase.Instance.Play(breakSoundKey);
        }
        gameObject.SetActive(false);

        yield return null;
    }

    #endregion
}
