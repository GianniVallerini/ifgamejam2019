﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldenButton : MonoBehaviour
{
    public LoadScene loadScene;
    public SpriteRenderer spriteRenderer;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        StartCoroutine(WinCor());
    }

    private IEnumerator WinCor()
    {
        spriteRenderer.enabled = false;
        yield return new WaitForSeconds(.5f);
        loadScene.Load();
    }
}
