﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CheckPointManager : SingletonDDOL<CheckPointManager>
{
    #region Fields

    public event Action Respawned;

    public string sceneToLoad = "insertSceneNameHere";
    public CheckPointSave lastCheckpointSaved;

    private AsyncOperation asyncLoadLevel;

    #endregion

    #region Unity Callbacks

    private void Start()
    {
        FindObjectOfType<NextLevel>().Initialize(0);
        SoundsDatabase.Instance.Play("InGameSoundtrack");
    }

    #endregion

    #region Methods

    public void Respawn()
    {
        StartCoroutine(LoadLevel());
    }

    private IEnumerator LoadLevel()
    {
        asyncLoadLevel = SceneManager.LoadSceneAsync(sceneToLoad, LoadSceneMode.Single);
        while (!asyncLoadLevel.isDone)
        {
            yield return null;
        }

        PlacePlayer();
    }

    private void PlacePlayer()
    {
        List<Checkpoint> checkpoints = FindObjectsOfType<Checkpoint>().ToList();
        Checkpoint respawnCheckpoint = checkpoints.Find(c => c.index == lastCheckpointSaved.checkpointId);

        Character player = FindObjectOfType<Character>();

        player.transform.position = respawnCheckpoint.spawnTransform.position;
        player.transform.localScale = respawnCheckpoint.spawnTransform.localScale;

        FindObjectOfType<FollowTarget>().ResetPosition();

        FindObjectOfType<NextLevel>().Initialize(lastCheckpointSaved.checkpointId);

        Respawned?.Invoke();
    }

    #endregion
}
