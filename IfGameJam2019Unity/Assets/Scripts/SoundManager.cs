﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : SingletonDDOL<SoundManager>
{
    #region Fields

    public AudioSource oneShotSource;
    public AudioSource continuousSource;

    #endregion

    #region Unity Callbacks

    #endregion

    #region Methods

    public void Play(SoundSample sample, float volumeModifier = 1)
    {
        if(sample.sampleType == SampleType.OneShot)
        {
            oneShotSource.PlayOneShot(sample.clip, sample.volume * volumeModifier);
        }
        else if (sample.sampleType == SampleType.Continuous)
        {
            continuousSource.clip = sample.clip;
            continuousSource.volume = sample.volume;
            continuousSource.Play();
        }
    }

    #endregion
}
