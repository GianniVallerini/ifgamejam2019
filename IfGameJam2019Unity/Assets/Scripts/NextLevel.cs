﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextLevel : MonoBehaviour
{
    public CheckPointSave checkPointSave;
    public int currRoomIndex = 0;
    public List<Room> rooms;
    public bool canTransit = true;

    public void Initialize(int roomIndex)
    {
        currRoomIndex = roomIndex;
        rooms[roomIndex].Activate();
        for (int i = 0; i < rooms.Count; i++)
        {
            if (i != roomIndex)
            {
                rooms[i].Deactivate();
            }
        }
    }

    public void GoToRoom(int index)
    {
        currRoomIndex = index;
        if (rooms.Count > currRoomIndex)
        {
            rooms[currRoomIndex].Activate();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (canTransit)
        {
            GoToRoom(checkPointSave.checkpointId + 1);
        }
    }

    private void GoToNextRoom()
    {
        currRoomIndex++;
        if(rooms.Count > currRoomIndex)
        {
            rooms[currRoomIndex].Activate();
        }
    }

    private IEnumerator WaitCor()
    {
        canTransit = false;
        yield return new WaitForSeconds(2f);
        canTransit = true;
    }
}
