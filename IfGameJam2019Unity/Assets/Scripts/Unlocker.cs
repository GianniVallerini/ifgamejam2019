﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unlocker : MonoBehaviour
{
    public event Action Unlock;

    [Button("Unlock")]
    public void CallUnlock()
    {
        Unlock?.Invoke();
    }
}
