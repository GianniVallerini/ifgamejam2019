﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Accelerometer : MonoBehaviour
{
    #region Fields

    public Image minusFiller;
    public Image plusFiller;

    #endregion

    #region Unity Callbacks

    private void Start()
    {
        AccelerationManager.instance.AccelerationChanged += UpdateAccelerometerSlider;
    }

    #endregion

    #region Methods

    private void UpdateAccelerometerSlider(float normalizedValue)
    {
        plusFiller.fillAmount = Mathf.InverseLerp(.5f, 0f, normalizedValue);
        minusFiller.fillAmount = Mathf.InverseLerp(.5f, 1f, normalizedValue);
    }

    #endregion
}
