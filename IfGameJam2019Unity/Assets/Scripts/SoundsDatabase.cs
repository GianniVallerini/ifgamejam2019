﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "Sounds Database", menuName = "Sound/New Sound Database")]
public class SoundsDatabase : SerializedSingletonScriptable<SoundsDatabase>
{
    #region Fields

    [DictionaryDrawerSettings(DisplayMode = DictionaryDisplayOptions.OneLine, IsReadOnly = false, KeyLabel = "Sound Key", ValueLabel = "Sound Sample")]
    public Dictionary<string, SoundSample> soundsDictionaries = new Dictionary<string, SoundSample>();

    #endregion

    #region Unity Callbacks

    #endregion

    #region Methods

    public void Play(string soundKey, float volumeModifier = 1)
    {
        if(soundsDictionaries.ContainsKey(soundKey))
        {
            if(soundsDictionaries[soundKey] != null)
            {
                soundsDictionaries[soundKey].Play(volumeModifier);
            }
            else
            {
                Debug.Log("Sound Sample not set for " + soundKey);
            }
        }
        else
        {
            Debug.Log(soundKey + " not present in dictionary");
        }
    }

    #endregion
}
