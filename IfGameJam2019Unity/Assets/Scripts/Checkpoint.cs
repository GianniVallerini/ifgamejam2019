﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Checkpoint : MonoBehaviour
{
    #region Fields

    public CheckPointSave checkpointSave;
    public Transform spawnTransform;
    public Collider2D triggerCollider;
    public int index = 0;

    #endregion

    #region Unity Callbacks

    private void OnDrawGizmos()
    {
        if (triggerCollider != null)
        {
            Color gizmoColor = Color.green;
            gizmoColor.a = .6f;
            Gizmos.color = gizmoColor;

            Gizmos.DrawCube(triggerCollider.bounds.center + new Vector3(triggerCollider.offset.x,
                                                                        triggerCollider.offset.y, 0), triggerCollider.bounds.size);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        checkpointSave.checkpointId = index;
    }

    #endregion

    #region Methods

    #endregion
}
