﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneAfterSec : MonoBehaviour
{
    public string levelScene = "Level2";
    public string creditsScene = "Credits";
    public bool loading = false;

    private void Start()
    {
        SoundsDatabase.Instance.Play("MenuSoundtrack");
    }

    public void LoadLevel()
    {
        if (!loading)
        {
            SceneManager.LoadSceneAsync(levelScene);
            loading = true;
        }
    }

    public void LoadCredits()
    {
        if (!loading)
        {
            SceneManager.LoadSceneAsync(creditsScene);
            loading = true;
        }
    }
}
