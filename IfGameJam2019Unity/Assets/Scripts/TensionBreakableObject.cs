﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class TensionBreakableObject : Unlocker
{
    #region Fields

    public string breakSoundKey = "";

    [Range(0f,1f)]
    public float threshold = 0.5f;
    public ThresholdKeyword keyword = ThresholdKeyword.EqualTo;
    public bool isActive = true;
    public float shakestrenght = 1f;
    public float shakeTime = 1f;

    private float currentNormalizedAcceleration = .5f;
    private bool isBreaking = false;

    #endregion

    #region Unity Callbacks

    private void Start()
    {
        AccelerationManager.instance.AccelerationChanged += UpdateCurrentAcceleration;
        StartCoroutine(ShakerCor());
    }

    #endregion

    #region Methods

    public void UpdateCurrentAcceleration(float newAcceleration)
    {
        if (isActive)
        {
            currentNormalizedAcceleration = newAcceleration;
            switch (keyword)
            {
                case ThresholdKeyword.EqualTo:
                    if (currentNormalizedAcceleration == threshold)
                    {
                        Break();
                    }
                    break;
                case ThresholdKeyword.LessThan:
                    if (currentNormalizedAcceleration <= threshold)
                    {
                        Break();
                    }
                    break;
                case ThresholdKeyword.MoreThan:
                    if (currentNormalizedAcceleration >= threshold)
                    {
                        Break();
                    }
                    break;
            }
        }
    }

    private void Break()
    {
        if (!isBreaking)
        {
            CallUnlock();
            StartCoroutine(DestroyCor());
        }
    }

    private IEnumerator DestroyCor()
    {
        isBreaking = true;
        if (breakSoundKey != "")
        {
            SoundsDatabase.Instance.Play(breakSoundKey);
        }
        AccelerationManager.instance.AccelerationChanged -= UpdateCurrentAcceleration;
        yield return new WaitForSeconds(.2f);

        gameObject.SetActive(false);
    }
    
    private IEnumerator ShakerCor()
    {
        Vector3 startingPos = transform.position;

        while (true)
        {
            yield return StartCoroutine(ShakeCor(startingPos));
            yield return new WaitForSeconds(shakeTime);
        }
    }

    private IEnumerator ShakeCor(Vector3 startingPos)
    {
        float t = 0;
        float maxT = .5f;
        while(t < maxT)
        {
            t += Time.deltaTime;
            transform.position = startingPos + new Vector3(UnityEngine.Random.Range(-shakestrenght, shakestrenght),
                                                           UnityEngine.Random.Range(-shakestrenght, shakestrenght),
                                                           0);
            yield return null;
            t += Time.deltaTime;
            yield return null;
        }

        transform.position = startingPos;
    }

    #endregion
}

public enum ThresholdKeyword
{
    MoreThan,
    LessThan,
    EqualTo,
}
