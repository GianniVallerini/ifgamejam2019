﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class DeathZone : MonoBehaviour
{
    #region Fields

    public Collider2D triggerCollider;

    #endregion

    #region Unity Callbacks

    private void OnDrawGizmos()
    {
        if (triggerCollider != null)
        {
            Color gizmoColor = Color.red;
            gizmoColor.a = .6f;
            Gizmos.color = gizmoColor;

            Gizmos.DrawCube(triggerCollider.bounds.center, triggerCollider.bounds.size);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Character character = collision.GetComponent<Character>();
        if(character != null)
        {
            character.Die();
        }
    }

    #endregion

    #region Methods

    #endregion
}
