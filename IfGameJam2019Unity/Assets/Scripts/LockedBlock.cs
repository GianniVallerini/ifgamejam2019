﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class LockedBlock : MonoBehaviour
{
    #region Fields

    public Unlocker unlocker;
    public bool startsLocked = true;

    private Rigidbody2D rb;

    #endregion

    #region Unity Callbacks

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        if (startsLocked)
        {
            if (unlocker != null)
            {
                unlocker.Unlock += Unlock;
            }
        }
        else
        {
            Unlock();
        }
    }

    #endregion

    #region Methods

    public void Unlock()
    {
        rb.constraints = RigidbodyConstraints2D.None;
    }

    #endregion
}
