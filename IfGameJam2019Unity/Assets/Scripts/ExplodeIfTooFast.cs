﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodeIfTooFast : MonoBehaviour
{
    #region Fields

    public Rigidbody2D rb;
    public float necessarySpeed = 1f;
    public GameObject explosionParticle;
    public LayerMask iDamageableMask;
    public string soundKey = "";

    #endregion

    #region Unity Callbacks

    private void OnCollisionEnter2D(Collision2D collision)
    {
        int objectHitLayer = collision.gameObject.layer;
        if (LayerRecon.IsInLayerMask(objectHitLayer, iDamageableMask))
        {
            if (rb != null)
            {
                if (rb.velocity.magnitude >= necessarySpeed)
                {
                    Break();
                }
            }
        }
    }

    #endregion

    #region Methods

    private void Break()
    {
        if(explosionParticle != null)
        {
            Instantiate(explosionParticle, transform.position, Quaternion.identity);
        }
        if(soundKey != "")
        {
            SoundsDatabase.Instance.Play(soundKey);
        }

        Destroy(gameObject);
    }


    #endregion
}
