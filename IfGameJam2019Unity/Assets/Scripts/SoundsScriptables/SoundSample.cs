﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "sound", menuName = "Sound/New Sound Sample")]
public class SoundSample : ScriptableObject
{
    public SampleType sampleType;
    public AudioClip clip;
    [Range(0f, 1f)]
    public float volume = .8f;

    public void Play(float volumeModifier = 1)
    {
        SoundManager.instance.Play(this, volumeModifier);
    }
}

public enum SampleType
{
    OneShot,
    Continuous,
}
