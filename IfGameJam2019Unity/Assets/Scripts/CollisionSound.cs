﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionSound : MonoBehaviour
{
    public float relativeVelocityToMakeSound = 1f;
    public string soundKey;        

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.relativeVelocity.magnitude > relativeVelocityToMakeSound)
        {
            if (soundKey != null)
            {
                SoundsDatabase.Instance.Play(soundKey);
            }
        }
    }
}
