﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "checkpoint_save", menuName = "Checkpoint Save")]
public class CheckPointSave : ScriptableObject
{
    public int checkpointId = 0;
}
