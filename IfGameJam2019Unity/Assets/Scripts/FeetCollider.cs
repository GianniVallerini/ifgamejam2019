﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeetCollider : MonoBehaviour
{
    #region Fields

    public event Action Grounded;
    public event Action Lifted;

    public Collider2D detectionCollider;

    #endregion

    #region Unity Callbacks

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Grounded?.Invoke();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!detectionCollider.IsTouchingLayers())
        {
            Lifted?.Invoke();
        }
    }

    #endregion

    #region Methods

    #endregion
}
