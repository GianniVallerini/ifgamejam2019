﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Room : MonoBehaviour
{
    public event Action<Room> RoomActivated; 

    public Vector2 boundsX;
    public Vector2 boundsY;

    private List<AccelerationAffected> accelerationAffecteds = new List<AccelerationAffected>();
    private List<TensionBreakableObject> tensionBreakableObjects = new List<TensionBreakableObject>();

    private void Awake()
    {
        accelerationAffecteds = GetComponentsInChildren<AccelerationAffected>().ToList();
        tensionBreakableObjects = GetComponentsInChildren<TensionBreakableObject>().ToList();
    }

    public void Activate()
    {
        foreach(AccelerationAffected accelerationAffected in accelerationAffecteds)
        {
            AccelerationManager.instance.accelerationAffecteds.Add(accelerationAffected);
        }
        foreach (TensionBreakableObject tensionBreakableObject in tensionBreakableObjects)
        {
            tensionBreakableObject.isActive = true;
        }
        RoomActivated?.Invoke(this);
    }

    public void Deactivate()
    {
        foreach (AccelerationAffected accelerationAffected in accelerationAffecteds)
        {
            AccelerationManager.instance.accelerationAffecteds.Remove(accelerationAffected);
        }
        foreach (TensionBreakableObject tensionBreakableObject in tensionBreakableObjects)
        {
            tensionBreakableObject.isActive = false;
        }
    }
}
