﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class Character : MonoBehaviour, IDamageable
{
    #region Fields

    public float acceleration = 6f;
    public float deceleration = 4f;
    public float maxSpeed = 8f;
    public FeetCollider feetCollider;
    public Animator anim;
    public GameObject deathParticle;
    public SpriteRenderer spriteRenderer;

    private Vector3 currSpeed;
    public Vector3 CurrSpeed
    {
        get
        {
            return currSpeed;
        }
        set
        {
            if (currSpeed != value)
            {
                float xScale = 1;
                currSpeed = value;
                
                if(currSpeed.x > -movementThreshold && currSpeed.x < movementThreshold)
                {
                    currSpeed = new Vector3(0, currSpeed.y, 0);
                    anim.SetBool("IsStopped", true);
                }
                else if (currSpeed.x > movementThreshold)
                {
                    xScale = 1;
                    transform.localScale = new Vector3(xScale, 1, 1);
                    anim.SetBool("IsStopped", false);
                }
                else if (currSpeed.x < -movementThreshold)
                {
                    xScale = -1;
                    transform.localScale = new Vector3(xScale, 1, 1);
                    anim.SetBool("IsStopped", false);
                }
            }
        }
    }

    private Vector3 movementDirection;
    public Vector3 MovementDirection
    {
        get
        {
            return movementDirection;
        }
        set
        {
            if (movementDirection != value)
            {
                movementDirection = value;
            }
        }
    }

    private float movementThreshold = .1f;
    private Rigidbody2D rb;
    private float horizInput = 0;
    private Player rePlayer;
    private bool isOnTheGround = true;
    private bool isDying = false;
    private Damageable damageable = new Damageable(1);

    private readonly Vector3 rightVector = new Vector3(1, 1, 1);
    private readonly Vector3 leftVector = new Vector3(-1, 1, 1);

    #endregion

    #region Unity Callbacks

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        rePlayer = ReInput.players.GetPlayer(0);

        feetCollider.Grounded += SetIsOnTheGroundTrue;
        feetCollider.Lifted += SetIsOnTheGroundFalse;
    }

    private void Update()
    {
        horizInput = rePlayer.GetAxis(RewiredConsts.Action.Horizontal_Movement);

        if (isOnTheGround)
        {
            MovementDirection = horizInput * Vector3.right * Time.deltaTime * acceleration * 100;
        }
        else
        {
            MovementDirection = Vector3.zero;
        }
    }

    private void FixedUpdate()
    {
        Accelerate(MovementDirection);
        if(isOnTheGround && horizInput == 0)
        {
            Decelerate();
        }

        CurrSpeed = rb.velocity;
    }

    #endregion

    #region Methods

    public void Accelerate(Vector3 accelerationVector)
    {
        if ((Mathf.Sign(accelerationVector.x) == Mathf.Sign(CurrSpeed.x) && Mathf.Abs(CurrSpeed.x) < maxSpeed) ||
            Mathf.Sign(accelerationVector.x) != Mathf.Sign(CurrSpeed.x))
        {
            rb.AddForce(accelerationVector * Time.fixedDeltaTime, ForceMode2D.Impulse);
        }
    }

    public void Decelerate()
    {
        rb.velocity -= rb.velocity.normalized * deceleration * Time.fixedDeltaTime;
    }

    public void Die()
    {
        if (!isDying)
        {
            StartCoroutine(DeathCor());
        }
    }

    private IEnumerator DeathCor()
    {
        isDying = true;
        Instantiate(deathParticle, transform);
        spriteRenderer.enabled = false;
        yield return new WaitForSeconds(.5f);
        CheckPointManager.instance.Respawn();
        isDying = false;
    }

    private void SetIsOnTheGroundTrue()
    {
        isOnTheGround = true;
        anim.SetBool("InAir", false);
    }

    private void SetIsOnTheGroundFalse()
    {
        isOnTheGround = false;
        anim.SetBool("InAir", true);
    }

    public void TakeDamage(int damage)
    {
        damageable.TakeDamage(damage);
    }

    public void InstaDestroy()
    {
        damageable.InstaDestroy();
        Die();
    }

    #endregion
}
