﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class AccelerationAffected : MonoBehaviour
{
    #region Fields

    public float multiplier = 1f;
    public bool registerAtStart = false;

    private Rigidbody2D rb;

    #endregion

    #region Unity Callbacks

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        if (registerAtStart)
        {
            AccelerationManager.instance.accelerationAffecteds.Add(this);
        }
    }

    private void OnDestroy()
    {
        AccelerationManager.instance.accelerationAffecteds.Remove(this);
    }

    #endregion

    #region Methods

    public void Accelerate(Vector3 acceleration)
    {
        if (rb != null)
        {
            rb.AddForce(acceleration * multiplier, ForceMode2D.Force);
        }
    }

    #endregion
}
