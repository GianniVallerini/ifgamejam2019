﻿using Rewired;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class AccelerationManager : Singleton<AccelerationManager>
{
    #region Fields

    public event Action<float> AccelerationChanged;

    public float minimumAccelerationScale = -1.1f;
    public float maximumAccelerationScale = 1f;
    public float accelerationChangeSpeed = 1f;
    public float decelerationChangeSpeed = 1f;
    public float baseAccelerationNormalized = .5f;

    [HideInInspector] public List<AccelerationAffected> accelerationAffecteds;

    private Player rePlayer;
    private float scrollDelta = 0;
    private float currNormalizedAccelerationScale = 0.5f;
    public float CurrNormalizedAccelerationScale
    {
        get
        {
            return currNormalizedAccelerationScale;
        }
        set
        {
            if (currNormalizedAccelerationScale != value)
            {
                currNormalizedAccelerationScale = value;
                if (currNormalizedAccelerationScale < 0)
                {
                    currNormalizedAccelerationScale = 0;
                }
                if (currNormalizedAccelerationScale > 1)
                {
                    currNormalizedAccelerationScale = 1;
                }

                AccelerationChanged?.Invoke(currNormalizedAccelerationScale);
            }
        }
    }

    #endregion

    #region Unity Callbacks

    private void Start()
    {
        rePlayer = ReInput.players.GetPlayer(0);
        CurrNormalizedAccelerationScale = baseAccelerationNormalized;
    }

    private void Update()
    {
        float scroll = rePlayer.GetAxis(RewiredConsts.Action.Gravity_Scroll);
        if (scroll != 0)
        {
            if(scroll < 0 && CurrNormalizedAccelerationScale > baseAccelerationNormalized ||
               scroll > 0 && CurrNormalizedAccelerationScale < baseAccelerationNormalized)
            {
                FlatAcceleration();
            }
            ChangeAcceleration(scroll * accelerationChangeSpeed * Time.deltaTime);
        }
        else
        {
            FlatAcceleration();
        }

        ApplyAcceleration();
    }

    #endregion

    #region Methods

    private void ApplyAcceleration()
    {
        foreach(AccelerationAffected accelerationAffected in accelerationAffecteds)
        {
            accelerationAffected.Accelerate(Vector3.up * Mathf.Lerp(minimumAccelerationScale, maximumAccelerationScale,
                                                                    CurrNormalizedAccelerationScale));
        }
    }

    private void FlatAcceleration()
    {
        if (CurrNormalizedAccelerationScale != baseAccelerationNormalized)
        {
            if (CurrNormalizedAccelerationScale > baseAccelerationNormalized)
            {
                CurrNormalizedAccelerationScale -= decelerationChangeSpeed * Time.deltaTime;
            }

            if (CurrNormalizedAccelerationScale < baseAccelerationNormalized)
            {
                CurrNormalizedAccelerationScale += decelerationChangeSpeed * Time.deltaTime;
            }

            if (CurrNormalizedAccelerationScale > baseAccelerationNormalized - decelerationChangeSpeed * Time.deltaTime &&
               CurrNormalizedAccelerationScale < baseAccelerationNormalized + decelerationChangeSpeed * Time.deltaTime)
            {
                CurrNormalizedAccelerationScale = baseAccelerationNormalized;
            }
        }
    }

    private void ChangeAcceleration(float change)
    {
        CurrNormalizedAccelerationScale += change;
    }

    #endregion
}
